package com.wewish.mvc;

import com.wewish.security.MyUserDetails;
import com.wewish.service.CmsUserService;
import com.wewish.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by diaozhengxin on 2015/7/1.
 */

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    CmsUserService cmsUserService;

    @Autowired
    MyUserDetailsService myUserDetailsService;

    /**
     * 仅作为拦截器，跳转到login.jsp界面
     * @param model
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String login(ModelMap model){
        return "login";
    }

    /**
     * 登陆成功界面，会根据不同权限显示对应功能，权限控制由spring security完成，具体见security配置文件
     * @param model
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String loginValidate(ModelMap model) {
        MyUserDetails myUserDetails = (MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Iterator<? extends GrantedAuthority> iterator = myUserDetails.getAuthorities().iterator();
        List<String> roles = new ArrayList<String>();
        while(iterator.hasNext()){
            roles.add(iterator.next().toString());
        }
        model.addAttribute("is_admin", roles.contains("ROLE_ADMIN"));
        model.addAttribute("is_user", roles.contains("ROLE_USER"));
        return "index";
    }

}
