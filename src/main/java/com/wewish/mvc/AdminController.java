package com.wewish.mvc;

import com.wewish.entity_cms.CmsUser;
import com.wewish.entity_cms.Role;
import com.wewish.service.CmsUserService;
import com.wewish.tools.ReturnCode;
import com.wewish.vo.BaseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sjx on 15/7/25.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private CmsUserService cmsUserService;

    /**
     * 列出所有的cmsUser，以及所有的权限。
     * @param mv
     * @return
     */
    @RequestMapping(value="cmsusers")
    public String getUserList(ModelMap mv) {
        List<CmsUser> cmsUsers = cmsUserService.getList();
        List<Map> res = new ArrayList<Map>();
        for(CmsUser cmsUser:cmsUsers){
            Map tmp = new HashMap();
            tmp.put("cmsuser", cmsUser);
            List<Role> roles = cmsUserService.getRolesOfUser(cmsUser.getId());
            List<String> rolestring = new ArrayList<String>();
            for(Role role:roles){
                rolestring.add(role.getRoleType().toString());
            }
            tmp.put("is_admin", rolestring.contains("ROLE_ADMIN"));
            tmp.put("is_user", rolestring.contains("ROLE_USER"));
            res.add(tmp);
        }
        mv.addAttribute("cmsuserlist", res);
        return "admin/cmsusers";
    }

    /**
     * 对某个用户添加某个权限
     * @param username
     * @param roletype
     * @return 若用户不存在或权限不存在则返回status为FAIL，否则为SUCCESS。
     */
    @ResponseBody
    @RequestMapping(value="add_role", method = RequestMethod.POST)
    public BaseVo addRole(@RequestParam("user") String username, @RequestParam("role") String roletype) {
        BaseVo baseVo = new BaseVo(ReturnCode.FAIL);
        Role role = cmsUserService.getRoleByRoletype(roletype);
        CmsUser cmsUser = cmsUserService.getUserByUsername(username);
        if(role!=null && cmsUser!=null){
            cmsUserService.addUserRole(cmsUser, role);
            baseVo.setStatus(ReturnCode.SUCCESS);
        }
        return baseVo;
    }

    /**
     * 对某个用户删除某个权限
     * @param username
     * @param roletype
     * @return 若用户不存在或权限不存在则返回status为FAIL，否则为SUCCESS。
     */
    @ResponseBody
    @RequestMapping(value="del_role", method = RequestMethod.POST)
    public BaseVo delRole(@RequestParam("user") String username, @RequestParam("role") String roletype){
        BaseVo baseVo = new BaseVo(ReturnCode.FAIL);
        Role role = cmsUserService.getRoleByRoletype(roletype);
        CmsUser cmsUser = cmsUserService.getUserByUsername(username);
        if(role!=null && cmsUser!=null){
            cmsUserService.delUserRole(cmsUser, role);
            baseVo.setStatus(ReturnCode.SUCCESS);
        }
        return baseVo;
    }

    /**
     * 仅作为拦截器，跳转到addcmsuser页面
     * @param model
     * @return
     */
    @RequestMapping(value="add_cmsuser")
    public String add_Cmsuser(ModelMap model){
        return "admin/addcmsuser";
    }

    /**
     * 添加一个CmsUser，该接口由addcmsuser页面进行提交
     * @param username
     * @param password
     * @param state
     * @return 若用户名已存在，则返回FAIL。否则返回SUCCESS（200）。
     */
    @ResponseBody
    @RequestMapping(value="addcmsuser", method = RequestMethod.POST)
    public BaseVo addCmsuser(@RequestParam("username") String username,
                             @RequestParam("password") String password,
                             @RequestParam("state") int state){
        BaseVo baseVo = new BaseVo(ReturnCode.FAIL);
        CmsUser cmsUser = cmsUserService.getUserByUsername(username);
        if(cmsUser==null) {
            cmsUser = new CmsUser();
            cmsUser.setUsername(username);
            cmsUser.setPassword(password);
            cmsUser.setState(state);
            cmsUserService.add(cmsUser);
            baseVo.setStatus(ReturnCode.SUCCESS);
        }
        return baseVo;
    }

}
