package com.wewish.mvc;

import com.wewish.entity_tbl.Gift;
import com.wewish.entity_tbl.TblOrder;
import com.wewish.entity_tbl.TblUser;
import com.wewish.tools.OrderStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by markwhat on 15/8/17.
 */
@Controller
@RequestMapping("/")
public class RootController {
    @RequestMapping(method = RequestMethod.GET)
    public String login(ModelMap model){
        return "redirect:login";
    }

    @RequestMapping(value="clientaccount",method = RequestMethod.GET)
    public String getClintAccount(ModelMap mv) {
        return "page_clientinfo";
    }

    @RequestMapping(value="loglist",method = RequestMethod.GET)
    public String getLogList(ModelMap mv) {
        return "page_loglist";
    }

    @RequestMapping(value="pushtab",method = RequestMethod.GET)
    public String getPushTab(ModelMap mv) {
        return "page_pushmsg";
    }

}

