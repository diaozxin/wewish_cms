package com.wewish.mvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.wewish.entity_tbl.*;
import com.wewish.entity_cms.CmsOrder;
import com.wewish.security.MyUserDetails;
import com.wewish.service.*;
import com.wewish.tools.OrderStatus;
import com.wewish.tools.ReturnCode;
import com.wewish.vo.BaseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by sjx on 15/7/13.
 */
@Controller
@RequestMapping(value = "orderlist")
public class TblOrderListController {
    //private static Logger logger = Logger.getLogger(OrderListController.class);
    @Autowired
    private TblOrderService tblorderService;

    @Autowired
    private WishService wishService;

    @Autowired
    private CmsOrderService cmsOrderService;

    @Autowired
    private TblUserService tblUserService;

    @Autowired
    private CmsUserService cmsUserService;

    @Autowired
    private AccountService accountService;

    @Autowired
    MyUserDetailsService myUserDetailsService;

    /**
     * 列出全部的status不是COMPLETED的tbl订单。（已下单的订单加入CmsOrder表中)
     * @return 跳转到需要运营人员下单或更改订单状态的页面
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getOrders(RedirectAttributes attributes){
        return "redirect:/orderlist/pager?id=&completed=&mobile=&pagesize=&offset=";
    }

    @RequestMapping(value = "pager", method = RequestMethod.GET)
    public String getOrderPager(ModelMap model,
                                @RequestParam("id") Integer id,
                                @RequestParam("mobile") String mobile,
                                @RequestParam("completed") Boolean completed,
                                @RequestParam("pagesize") Integer pagesize,
                                @RequestParam("offset") Integer offset){
        TblUser tblUser = null;
        if(null != id){
            tblUser = tblUserService.getTblUser(id);
        }
        if(null == mobile || "".equals(mobile)){
            mobile = "";
        }
        if(null == completed){
            completed = false;
        }
        if(null == pagesize || pagesize < 1 || pagesize > 10){
            pagesize = 10;
        }
        if(null == offset || offset < 1){
            offset = 1;
        }
        Pagers<TblOrder> orderPagers = tblorderService.getOrderPager(tblUser, completed, mobile, pagesize,offset);
        List<TblOrder> tblOrders = orderPagers.getDatas();
        List orderAndGiftList = new ArrayList();
        for(TblOrder tblOrder:tblOrders){
            Map node = new HashMap();
            Gift gift = wishService.getGift(tblOrder.getWish());
            node.put("order", tblOrder);
            node.put("gift", gift);
            node.put("status", OrderStatus.getStatusDescribe(tblOrder.getStatus()));
            orderAndGiftList.add(node);
        }
        model.put("orderList", orderAndGiftList);
        model.put("pageInfo", orderPagers);
        return "tblOrderList";
    }

    /**
     * 运营人员点击某条订单的记账按钮时，使用这个拦截器跳转到orderbought.ftl页面，用于运营人员的订单操作
     * @param mv
     * @param id 订单的ID
     * @return
     */
    @RequestMapping(value="orderboughtform", method = RequestMethod.GET)
    public String orderBoughtForm(ModelMap mv, @RequestParam("id") int id){
        TblOrder tblorder = tblorderService.get(id);

        if(tblorder == null){
            mv.addAttribute("info","no such tbl_order id!");
            return "error";
        }
        Wish wish = tblorder.getWish();
        int status = tblorder.getStatus();
        CmsOrder cmsOrder = cmsOrderService.getCmsOrderByTblOrder(tblorder);
        if(cmsOrder != null){
            mv.addAttribute("paidprice",cmsOrder.getPrice().toString());
            mv.addAttribute("detail",cmsOrder.getDetail());
        }else{
            mv.addAttribute("paidprice","");
            mv.addAttribute("detail","记录备注...");
        }
        Gift gift = wishService.getGift(wish);
        mv.addAttribute("id", id);//tblorder 中的订单 id
        mv.addAttribute("order",tblorder);
        mv.addAttribute("gift",gift);
        mv.addAttribute("status", status);
        mv.addAttribute("str_status", OrderStatus.getStatusDescribe(status));
        mv.addAttribute("nextstatus", OrderStatus.getPosibleNextStatus(status));
        mv.addAttribute("nextstatus_str", OrderStatus.getPosibleNextStatusStr(status));

        return "orderbought";
    }

    /**
     * 添加一个新的cmsOrder/  修改一个已经存在的cmsOrder的价格与描述/  修改一个已经存在的tblOrder状态
     * @param mv
     * @param id tblOrder的id，由此获得tblOrder，再由tblOrder决定是否已经存在对应的cmsOrder
     * @param price cmsOrder实际的价格，即购买商品的实际价格
     * @param detail 相关信息描述
     * @param nextstatus 修改tblOrder状态时的新状态
     * @return 返回一个提示页面，提示操作是否成功
     */
    @RequestMapping(value = "orderbought", method = RequestMethod.GET)
    public String orderBought(ModelMap mv,
                              @RequestParam("id") int id,
                              @RequestParam("price") String price,
                              @RequestParam("detail") String detail,
                              @RequestParam("nextstatus") int nextstatus){
        TblOrder tblOrder = tblorderService.get(id);
        if(tblOrder == null){
            mv.addAttribute("info","no such tbl_order id!");
            return "error";
        }
        CmsOrder cmsOrder = cmsOrderService.getCmsOrderByTblOrder(tblOrder);
        if(cmsOrder == null){
            cmsOrder = new CmsOrder();
            Timestamp createtime = new Timestamp(System.currentTimeMillis());
            cmsOrder.setCreateTime(createtime);
            cmsOrder.setTblOrderId(tblOrder.getId());
        }
        BigDecimal cmsPrice = new BigDecimal(price);
        BigDecimal tblPrice = tblOrder.getPrice();
        cmsOrder.setPrice(cmsPrice);
        MyUserDetails userDetails = (MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        cmsOrder.setCmsUser(cmsUserService.getUserByUsername(userDetails.getUsername())); //TODO 如果允许修改cmsOrder，则每次操作都需要记录在日志中
        cmsOrder.setDetail(detail);
        int cmp = tblPrice.compareTo(cmsPrice);
        if(1 == cmp){ //tblPrice>cmsPrice的情况下，将多余的钱退回account中
            accountService.updateAvailableOfTbluser(tblPrice.subtract(cmsPrice),tblOrder.getUser());
        }
        cmsOrderService.saveOrUpdate(cmsOrder);
        if(OrderStatus.isStatusValid(nextstatus)){
            tblOrder.setStatus(nextstatus);
            tblorderService.update(tblOrder);
        }
        mv.addAttribute("info","successed!");
        return "error";
    }
}
