package com.wewish.vo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Created by chenyuanjin on 15/6/18.
 */
public class BaseVo {
    protected  int status =200;

    protected  ObjectNode result ;

    public BaseVo(){
        ObjectMapper mapper = new ObjectMapper();
        result = mapper.createObjectNode();
    };

    public BaseVo(int s){
        ObjectMapper mapper = new ObjectMapper();
        result = mapper.createObjectNode();
        this.status = s;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int state) {
        this.status =  state;
    }

    public ObjectNode getResult() {
        return result;
    }

    public void setResult(ObjectNode result) {
        this.result = result;
    }
}
