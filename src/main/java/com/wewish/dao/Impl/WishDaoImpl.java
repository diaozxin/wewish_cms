package com.wewish.dao.Impl;

import com.wewish.dao.WishDao;
import com.wewish.entity_tbl.Wish;
import org.springframework.stereotype.Repository;

/**
 * Created by sjx on 15/7/14.
 */
@Repository
public class WishDaoImpl extends TblBaseDaoImpl<Wish> implements WishDao {

}
