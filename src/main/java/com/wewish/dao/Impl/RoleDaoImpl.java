package com.wewish.dao.Impl;

import com.wewish.dao.RoleDao;
import com.wewish.entity_cms.Role;
import com.wewish.entity_cms.RoleType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by diaozhengxin on 2015/6/30.
 */
@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao{
    @Override
    public List<Role> listRole() {
        return list("from Role");
    }

    @Override
    public Role getRoleByType(RoleType roleType){
        String hql = "from Role r where r.roleType = ?";
        Object[] args = {
                roleType
        };
        return (Role)queryObject(hql, args);
    }

}
