package com.wewish.dao.Impl;

import com.wewish.dao.AccountDao;
import com.wewish.entity_tbl.Account;
import com.wewish.entity_tbl.TblUser;
import org.springframework.stereotype.Repository;

/**
 * Created by sjx on 15/8/19.
 */
@Repository
public class AccountDaoImpl extends TblBaseDaoImpl<Account> implements AccountDao {
    @Override
    public Account getAccountOfTbluser(TblUser tblUser){
        String hql = "from Account a where a.user = ?";
        Object[] args = {
                tblUser
        };
        return (Account) this.queryObject(hql, args);
    }
}
