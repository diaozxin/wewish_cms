package com.wewish.dao.Impl;

import com.wewish.dao.TblOrderDao;
import com.wewish.entity_tbl.Pagers;
import com.wewish.entity_tbl.TblOrder;
import com.wewish.entity_tbl.TblUser;
import com.wewish.tools.OrderStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sjx on 15/7/13.
 */
@Repository
public class TblOrderDaoImpl extends TblBaseDaoImpl<TblOrder> implements TblOrderDao {

    @Override
    public List<TblOrder> getOrderList(){
        String hql = "from TblOrder where status <> ?";
        Object[] args = {
                OrderStatus.COMPLETED
        };
        return this.list(hql, args);
    }

    @Override
    public List<TblOrder> getOrderListByUser(TblUser user){
        String hql = "from TblOrder where user = ? and status <> ?";
        Object[] args = {
                user,
                OrderStatus.COMPLETED
        };
        return this.list(hql, args);
    }

    @Override
    public List<TblOrder> getOrderCompletedList(){
        String hql = "from TblOrder where status = ?";
        Object[] args = {
                OrderStatus.COMPLETED
        };
        return this.list(hql, args);
    }

    @Override
    public List<TblOrder> getOrderCompletedListByUser(TblUser user){
        String hql = "from TblOrder where user = ? and status = ?";
        Object[] args = {
                user,
                OrderStatus.COMPLETED
        };
        return this.list(hql, args);
    }

    @Override
    public Pagers<TblOrder> getOrderPager(int pagesize, int offset){
        String hql = "from TblOrder where status <> ?";
        Object[] args = {
                OrderStatus.COMPLETED
        };
        return this.find(hql, args, pagesize, offset);
    }

    @Override
    public Pagers<TblOrder> getOrderPagerByUser(int pagesize, int offset, TblUser user){
        String hql = "from TblOrder where user = ? and status <> ?";
        Object[] args = {
                user,
                OrderStatus.COMPLETED
        };
        return this.find(hql, args, pagesize, offset);
    }

    @Override
    public Pagers<TblOrder> getOrderCompletedPager(int pagesize, int offset){
        String hql = "from TblOrder where status = ?";
        Object[] args = {
                OrderStatus.COMPLETED
        };
        return this.find(hql, args, pagesize, offset);
    }

    @Override
    public Pagers<TblOrder> getOrderCompletedPagerByUser(int pagesize, int offset, TblUser user){
        String hql = "from TblOrder where user = ? and status = ?";
        Object[] args = {
                user,
                OrderStatus.COMPLETED
        };
        return this.find(hql, args, pagesize, offset);
    }

}
