package com.wewish.dao.Impl;

import com.wewish.dao.CmsUserDao;
import com.wewish.entity_cms.CmsUser;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Zhengxin on 15/6/28.
 */
@Repository
public class CmsUserDaoImpl extends BaseDaoImpl<CmsUser> implements CmsUserDao {
    @Override
    public CmsUser getUserByUsername(String username) {

        String hql = "from CmsUser u where u.username = ?";

        Object[] args ={
                username
        };

        CmsUser cmsUser = (CmsUser) queryObject(hql,args);

        return cmsUser;
    }

    @Override
    public List<CmsUser> cmsUserList() {
        String hql = "from CmsUser";
        return this.list(hql);
    }
}
