package com.wewish.dao.Impl;

import com.wewish.dao.GiftDao;
import com.wewish.entity_tbl.Gift;
import com.wewish.entity_tbl.Wish;
import org.springframework.stereotype.Repository;

/**
 * Created by chenyuanjin on 15/6/20.
 */
@Repository
public class GiftDaoImpl extends TblBaseDaoImpl<Gift> implements GiftDao {
    @Override
    public Gift getGift(Wish wish) {
        String hql = "from Gift where wish = ?";
        Object[] args = {
                wish
        };
        return (Gift)this.queryObject(hql,args);
    }
}
