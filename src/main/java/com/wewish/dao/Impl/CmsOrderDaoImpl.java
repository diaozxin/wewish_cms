package com.wewish.dao.Impl;

import com.wewish.dao.CmsOrderDao;
import com.wewish.entity_cms.CmsOrder;
import com.wewish.entity_tbl.TblOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sjx on 15/7/21.
 */
@Repository
public class CmsOrderDaoImpl extends BaseDaoImpl<CmsOrder> implements CmsOrderDao {
    @Override
    public List<CmsOrder> getAllCmsOrder(){
        String hql = "from CmsOrder";
        return this.list(hql);
    }

    @Override
    public CmsOrder getCmsOrderByTblOrder(TblOrder tblOrder) {
        String hql = "from CmsOrder where tblOrderId = ?";
        Object[] args = {
                tblOrder.getId()
        };
        return (CmsOrder) this.queryObject(hql, args);
    }
}
