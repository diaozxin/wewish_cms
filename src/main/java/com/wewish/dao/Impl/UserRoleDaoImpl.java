package com.wewish.dao.Impl;

import com.wewish.dao.UserRoleDao;
import com.wewish.entity_cms.CmsUser;
import com.wewish.entity_cms.Role;
import com.wewish.entity_cms.UserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sjx on 15/7/28.
 */
@Repository
public class UserRoleDaoImpl extends BaseDaoImpl<UserRole> implements UserRoleDao {
    @Override
    public void deleteRoleUsers(int rid) {
        update("delete UserRole ur where ur.role.id=?", new Object[]{rid});
    }

    @Override
    public List<Role> listUserRoles(int uid) {
        String hql = "select ur.role from UserRole ur where ur.cmsUser.id = ?";
        return this.getSession().createQuery(hql).setParameter(0,uid).list();
    }

    @Override
    public UserRole getByUserAndRole(CmsUser cmsUser, Role role){
        String hql = "from UserRole ur where ur.role = ? and ur.cmsUser = ?";
        Object[] args = {
                role,
                cmsUser
        };
        return (UserRole)queryObject(hql, args);
    }
}
