package com.wewish.dao.Impl;

import com.wewish.dao.AddressDao;
import com.wewish.entity_tbl.Address;
import org.springframework.stereotype.Repository;

/**
 * Created by sjx on 15/7/14.
 */
@Repository
public class AddressDaoImpl extends TblBaseDaoImpl<Address> implements AddressDao {

}
