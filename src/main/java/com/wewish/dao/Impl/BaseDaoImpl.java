package com.wewish.dao.Impl;

import com.wewish.dao.IBaseDao;
import com.wewish.entity_tbl.Pagers;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by diaozhengxin on 2015/6/9.
 */
@Repository
public class BaseDaoImpl<T> implements IBaseDao<T> {

    private static Logger logger = Logger.getLogger(BaseDaoImpl.class);

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    /**
     * 创建一个Class的对象来获取泛型的class
     */
    private Class<?> clz;

    public Class<?> getClz() {
        if(clz==null) {
            //获取泛型的Class对象
            clz = ((Class<?>)
                    (((ParameterizedType)(this.getClass().getGenericSuperclass())).getActualTypeArguments()[0]));
        }
        return clz;
    }

    protected final Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public T add(T t) {
        getSession().save(t);
        return t;
    }

    @Override
    public void update(T t) {
        getSession().update(t);
    }

    @Override
    public void saveOrUpdate(T t){
        getSession().saveOrUpdate(t);

    }

    @Override
    public void delete(int id) {
        getSession().delete(this.load(id));
    }

    @Override
    public T load(int id) {
        return (T) getSession().load(getClz(), id);
    }

    @Override
    public T get(int id) {
        return (T) getSession().get(getClz(),id);
    }

    @Override
    public List<T> list(String hql, Object[] args, String sort, boolean desc) {

        hql = initSort(hql,sort,desc);
        Query query = getSession().createQuery(hql);
        setParameter(query,args);
        return query.list();
    }

    @Override
    public List<T> list(String hql, Object[] args) {
        return list(hql,args,null,false);
    }

    @Override
    public List<T> list(String hql) {
        return list(hql,null);
    }

    @Override
    public Pagers<T> find(String hql, Object[] args, String sort, boolean desc, int pageSize, int offset) {
        String countHql = getCount(hql);
        hql = initSort(hql,sort,desc);

        Query countQuery = getSession().createQuery(countHql);
        Query query = getSession().createQuery(hql).setMaxResults(pageSize).setFirstResult(offset);

        setParameter(countQuery,args);
        setParameter(query,args);

        Pagers<T> pagers = new Pagers<T>();
        pagers.setPageSize(pageSize);
        pagers.setOffSet(offset);
        pagers.setTotal(Integer.parseInt(countQuery.uniqueResult().toString()));
        pagers.setDatas(query.list());
        return pagers;
    }

    @Override
    public Pagers<T> find(String hql, Object[] args, int pageSize, int offset) {
        return find(hql,args,null,false,pageSize,offset);
    }

    @Override
    public Pagers<T> find(String hql, int pageSize, int offset) {
        return find(hql,null,pageSize,offset);
    }

    @Override
    public void update(String hql, Object[] args) {
        Query query = getSession().createQuery(hql);
        setParameter(query,args);
        query.executeUpdate();
    }

    @Override
    public void update(String hql) {
        update(hql,null);
    }

    @Override
    public Object queryObject(String hql, Object[] args) {
        Query query = getSession().createQuery(hql);
        setParameter(query,args);
        return query.uniqueResult();
    }

    @Override
    public Object queryObject(String hql) {
        return queryObject(hql,null);
    }

    @Override
    public List<T> listBySql(String sql, Object[] args, String sort, boolean desc) {
        sql = initSort(sql,sort,desc);
        Query query = getSession().createSQLQuery(sql);
        setParameter(query,args);
        return query.list();
    }

    @Override
    public List<T> listBySql(String sql, Object[] args) {
        return listBySql(sql,args,null,false);
    }

    @Override
    public List<T> listBySql(String sql) {
        return listBySql(sql,null);
    }

    @Override
    public Pagers<T> findBySql(String sql, Object[] args, Class<?> clz, boolean hasEntity, String sort, boolean desc, int pageSize, int offset) {

        String countHql = getCount(sql);
        sql = initSort(sql,sort,desc);

        SQLQuery countQuery = getSession().createSQLQuery(countHql);
        SQLQuery query = getSession().createSQLQuery(sql);

        query.setMaxResults(pageSize).setFirstResult(offset);

        setParameter(countQuery,args);
        setParameter(query,args);

        if(hasEntity){
            query.addEntity(clz);
        }else {
            query.setResultTransformer(Transformers.aliasToBean(clz));
        }

        Pagers<T> pagers = new Pagers<T>();
        pagers.setPageSize(pageSize);
        pagers.setOffSet(offset);
        pagers.setTotal(Integer.parseInt(countQuery.uniqueResult().toString()));
        pagers.setDatas(query.list());


        return pagers;
    }

    @Override
    public Pagers<T> findBySql(String sql, Object[] args, Class<?> clz, boolean hasEntity, int pageSize, int offset) {
        return findBySql(sql,args,clz,hasEntity,null,false,pageSize,offset);
    }

    @Override
    public Pagers<T> findBySql(String sql, Class<?> clz, boolean hasEntity, int pageSize, int offset) {
        return findBySql(sql,null,clz,hasEntity,pageSize,offset);
    }

    @Override
    public Object queryObjectBySql(String sql, Object[] args) {
        Query query = getSession().createQuery(sql);
        setParameter(query, args);
        return query.uniqueResult();
    }

    @Override
    public Object queryObjectBySql(String sql) {
        return queryObjectBySql(sql,null);
    }

    private String initSort(String hql,String sort,boolean desc){
        if(StringUtils.isNotBlank(sort)){
            hql += " order by " + sort;
        }

        if(desc){
            hql += " desc ";
        }

        return hql;
    }

    private void setParameter(Query query,Object[] args){
        if(args!=null && args.length > 0){
            int index = 0;
            for(Object arg:args){
                query.setParameter(index++,arg);
            }
        }
    }

    private void setPagers(Query query,int pageSize,int offSet){
        if(pageSize == 0) pageSize = 15;
        query.setFirstResult(offSet).setMaxResults(pageSize);
    }

    private String getCount(String hql){
        String e = hql.substring(hql.indexOf("from"));
        String c = "select count(*) " + e;
        c = c.replace("fetch","");
        return c;
    }

}
