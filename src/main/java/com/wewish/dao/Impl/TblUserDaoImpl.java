package com.wewish.dao.Impl;

import com.wewish.dao.TblUserDao;
import com.wewish.entity_tbl.TblUser;
import org.springframework.stereotype.Repository;

/**
 * Created by sjx on 15/7/14.
 */
@Repository
public class TblUserDaoImpl extends TblBaseDaoImpl<TblUser> implements TblUserDao {

    @Override
    public TblUser loadByMobile(String mobile) {
        String hql = "from TblUser where mobile = ?";
        Object[] args = {
                mobile
        };

        return (TblUser) this.queryObject(hql,args);
    }

    @Override
    public  TblUser loadByToken(String token) {
        String hql = "from TblUser where token = ?";
        Object[] args = {
                token
        };

        return (TblUser) this.queryObject(hql,args);
    }
}
