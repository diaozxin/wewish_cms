package com.wewish.dao;

import com.wewish.entity_tbl.Address;

/**
 * Created by sjx on 15/7/14.
 */
public interface AddressDao extends IBaseDao<Address> {
}
