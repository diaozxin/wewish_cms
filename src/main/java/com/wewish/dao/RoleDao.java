package com.wewish.dao;

import com.wewish.entity_cms.Role;
import com.wewish.entity_cms.RoleType;

import java.util.List;

/**
 * Created by diaozhengxin on 2015/6/30.
 */
public interface RoleDao extends IBaseDao<Role>{

    List<Role> listRole();

    Role getRoleByType(RoleType roleType);
}
