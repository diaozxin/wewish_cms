package com.wewish.dao;

import com.wewish.entity_cms.CmsOrder;
import com.wewish.entity_tbl.TblOrder;
import java.util.List;

/**
 * Created by sjx on 15/7/21.
 */
public interface CmsOrderDao extends IBaseDao<CmsOrder> {
    List<CmsOrder> getAllCmsOrder();
    CmsOrder getCmsOrderByTblOrder(TblOrder tblOrder);

}
