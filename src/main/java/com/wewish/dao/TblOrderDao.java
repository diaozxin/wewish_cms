package com.wewish.dao;

import com.wewish.entity_tbl.Pagers;
import com.wewish.entity_tbl.TblOrder;
import com.wewish.entity_tbl.TblUser;

import java.util.List;

/**
 * Created by sjx on 15/7/13.
 */
public interface TblOrderDao extends IBaseDao<TblOrder> {

    public List<TblOrder> getOrderList();

    public List<TblOrder> getOrderListByUser(TblUser user);

    public List<TblOrder> getOrderCompletedList();

    public List<TblOrder> getOrderCompletedListByUser(TblUser user);

    public Pagers<TblOrder> getOrderPager(int pagesize,int offset);

    public Pagers<TblOrder> getOrderPagerByUser(int pagesize, int offset, TblUser user);

    public Pagers<TblOrder> getOrderCompletedPager(int pagesize, int offset);

    public Pagers<TblOrder> getOrderCompletedPagerByUser(int pagesize, int offset, TblUser user);

}
