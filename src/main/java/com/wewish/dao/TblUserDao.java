package com.wewish.dao;

import com.wewish.entity_tbl.TblUser;

/**
 * Created by sjx on 15/7/14.
 */
public interface TblUserDao extends IBaseDao<TblUser> {

    public TblUser loadByMobile(String mobile);

    public TblUser loadByToken(String token);
}
