package com.wewish.dao;

import com.wewish.entity_tbl.Pagers;

import java.util.List;

/**
 * Created by diaozhengxin on 2015/6/9.
 */
public interface IBaseDao<T> {

    public T add(T t);
    public void update(T t);
    public void saveOrUpdate(T t);
    public void delete(int id);
    public T load(int id);
    public T get(int id);

    public List<T> list(String hql, Object[] args, String sort, boolean desc);
    public List<T> list(String hql, Object[] args);
    public List<T> list(String hql);

    public Pagers<T> find(String hql, Object[] args, String sort, boolean desc, int pageSize, int offset);
    public Pagers<T> find(String hql, Object[] args, int pageSize, int offset);
    public Pagers<T> find(String hql, int PageSize, int offset);

    public void update(String hql, Object[] args);
    public void update(String hql);

    public Object queryObject(String hql, Object[] args);
    public Object queryObject(String hql);

    public List<T> listBySql(String sql, Object[] args, String sort, boolean desc);
    public List<T> listBySql(String sql, Object[] args);
    public List<T> listBySql(String sql);

    public Pagers<T> findBySql(String sql, Object[] args, Class<?> clz, boolean hasEntity, String sort, boolean desc, int pageSize, int offset);
    public Pagers<T> findBySql(String sql, Object[] args, Class<?> clz, boolean hasEntity, int pageSize, int offset);
    public Pagers<T> findBySql(String sql, Class<?> clz, boolean hasEntity, int PageSize, int offset);

    public Object queryObjectBySql(String sql, Object[] args);
    public Object queryObjectBySql(String sql);


}
