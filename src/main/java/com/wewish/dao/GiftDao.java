package com.wewish.dao;

import com.wewish.entity_tbl.Gift;
import com.wewish.entity_tbl.Wish;

/**
 * Created by chenyuanjin on 15/6/20.
 */
public interface GiftDao extends IBaseDao<Gift> {
    public Gift getGift(Wish wish);
}
