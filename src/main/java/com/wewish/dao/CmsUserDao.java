package com.wewish.dao;


import com.wewish.entity_cms.CmsUser;

import java.util.List;

/**
 * Created by Zhengxin on 15/6/28.
 */

public interface CmsUserDao extends IBaseDao<CmsUser>{

    CmsUser getUserByUsername(String username);

    List<CmsUser> cmsUserList();


}
