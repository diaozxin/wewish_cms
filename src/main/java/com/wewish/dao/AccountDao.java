package com.wewish.dao;

import com.wewish.entity_tbl.Account;
import com.wewish.entity_tbl.TblUser;

import java.math.BigDecimal;

/**
 * Created by sjx on 15/8/19.
 */
public interface AccountDao extends IBaseDao<Account>{
    public Account getAccountOfTbluser(TblUser tblUser);
}
