package com.wewish.dao;

import com.wewish.entity_tbl.Wish;

/**
 * Created by sjx on 15/7/14.
 */
public interface WishDao extends IBaseDao<Wish> {

}
