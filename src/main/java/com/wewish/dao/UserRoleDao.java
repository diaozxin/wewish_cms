package com.wewish.dao;

import com.wewish.entity_cms.CmsUser;
import com.wewish.entity_cms.Role;
import com.wewish.entity_cms.UserRole;

import java.util.List;

/**
 * Created by sjx on 15/7/28.
 */
public interface UserRoleDao extends IBaseDao<UserRole> {

    void deleteRoleUsers(int rid);

    List<Role> listUserRoles(int uid);

    UserRole getByUserAndRole(CmsUser cmsUser, Role role);
}
