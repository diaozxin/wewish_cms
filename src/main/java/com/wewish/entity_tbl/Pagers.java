package com.wewish.entity_tbl;

import java.util.List;

/**
 * Created by diaozhengxin on 2015/6/9.
 */
public class Pagers<T> {

    private int pageSize;
    private int offSet;
    private int total;
    private List<T> datas;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getOffSet() {
        return offSet;
    }

    public void setOffSet(int offSet) {
        this.offSet = offSet;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }
}
