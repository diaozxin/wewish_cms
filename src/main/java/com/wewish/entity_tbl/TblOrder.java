package com.wewish.entity_tbl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wewish.tools.Json.JsonTimestampSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_order database table.
 * 
 */
@Entity
@Table(name="tbl_order")
@NamedQuery(name="TblOrder.findAll", query="SELECT o FROM TblOrder o")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","operations","roles","menus"})
public class TblOrder implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Timestamp createTime;
	private String description;
	private String detail;
	private BigDecimal price;
	private int status;
	private Address address;
	private TblUser user;
	private Wish wish;

	public TblOrder() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonSerialize(using=JsonTimestampSerializer.class)
	@Column(name="create_time")
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}


	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	//uni-directional many-to-one association to Address
	@ManyToOne
	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}


	//uni-directional many-to-one association to User
	@ManyToOne
	public TblUser getUser() {
		return this.user;
	}

	public void setUser(TblUser user) {
		this.user = user;
	}


	//uni-directional one-to-one association to Wish
	@OneToOne
	public Wish getWish() {
		return this.wish;
	}

	public void setWish(Wish wish) {
		this.wish = wish;
	}

}