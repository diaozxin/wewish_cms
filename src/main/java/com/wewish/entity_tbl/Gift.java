package com.wewish.entity_tbl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wewish.tools.Json.JsonTimestampSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_gift database table.
 * 
 */
@Entity
@Table(name="tbl_gift")
@NamedQuery(name="Gift.findAll", query="SELECT g FROM Gift g")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","operations","roles","menus"})
public class Gift implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Timestamp createTime;
	private String description;
	private String details;
	private BigDecimal price;
	private String url;
	private Wish wish;

	public Gift() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonSerialize(using=JsonTimestampSerializer.class)
	@Column(name="create_time")
	@JsonIgnore
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}


	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	//uni-directional many-to-one association to Wish
	@JsonIgnore
	@OneToOne
	public Wish getWish() {
		return this.wish;
	}

	public void setWish(Wish wish) {
		this.wish = wish;
	}

}