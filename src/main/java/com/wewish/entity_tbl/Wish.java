package com.wewish.entity_tbl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wewish.tools.Json.JsonTimestampSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_wish database table.
 * 
 */
@Entity
@Table(name="tbl_wish")
@NamedQuery(name="Wish.findAll", query="SELECT w FROM Wish w")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","operations","roles","menus"})
public class Wish implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String comment;
	private Timestamp createTime;
	private BigDecimal currentMoney;
	private Timestamp endTime;
	private Timestamp startTime;
	private int status;
	private BigDecimal totalMoney;
	private int type;

	@JsonIgnore
	private TblUser user;

	public Wish() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@JsonSerialize(using=JsonTimestampSerializer.class)
	@Column(name="create_time")
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


	@Column(name="current_money")
	public BigDecimal getCurrentMoney() {
		return this.currentMoney;
	}

	public void setCurrentMoney(BigDecimal currentMoney) {
		this.currentMoney = currentMoney;
	}

	@JsonSerialize(using=JsonTimestampSerializer.class)
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_time")
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	@JsonSerialize(using=JsonTimestampSerializer.class)
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_time")
	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Column(name="total_money")
	public BigDecimal getTotalMoney() {
		return this.totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}


	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}


	//uni-directional many-to-one association to User
	@JsonIgnore
	@ManyToOne
	public TblUser getUser() {
		return this.user;
	}

	public void setUser(TblUser user) {
		this.user = user;
	}


}