package com.wewish.entity_tbl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wewish.tools.Json.JsonTimestampSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_account database table.
 * 
 */
@Entity
@Table(name="tbl_account")
@NamedQuery(name="Account.findAll", query="SELECT a FROM Account a")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","operations","roles","menus"})
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private BigDecimal available;
	private BigDecimal blocked;
	private Timestamp createTime;
	private BigDecimal total;
	private int type;
	private TblUser user;

	public Account() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getAvailable() {
		return available;
	}

	public void setAvailable(BigDecimal available) {
		this.available = available;
	}

	public BigDecimal getBlocked() {
		return blocked;
	}

	public void setBlocked(BigDecimal blocked) {
		this.blocked = blocked;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@JsonIgnore
	@JsonSerialize(using=JsonTimestampSerializer.class)
	@Column(name="create_time")
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@JsonIgnore
	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}


	//uni-directional one-to-one association to User
	@JsonIgnore
	@OneToOne
	public TblUser getUser() {
		return this.user;
	}

	public void setUser(TblUser tblUser) {
		this.user = tblUser;
	}

}