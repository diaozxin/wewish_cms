package com.wewish.entity_tbl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wewish.tools.Json.JsonTimestampSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the tbl_address database table.
 * 
 */
@Entity
@Table(name="tbl_address")
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address a")
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","operations","roles","menus"})
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String address;
	private Timestamp createTime;
	private String phone;
	private int postcode;
	private String recipient;
	private int status;
	private TblUser user;

	public Address() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@JsonIgnore
	@JsonSerialize(using=JsonTimestampSerializer.class)
	@Column(name="create_time")
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}


	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public int getPostcode() {
		return this.postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}


	public String getRecipient() {
		return this.recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	//uni-directional many-to-one association to CmsUser
	@JsonIgnore
	@ManyToOne
	public TblUser getUser() {
		return this.user;
	}

	public void setUser(TblUser user) {
		this.user = user;
	}

}