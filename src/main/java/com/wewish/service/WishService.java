package com.wewish.service;

import com.wewish.entity_tbl.Gift;
import com.wewish.entity_tbl.Wish;

/**
 * Created by sjx on 15/7/14.
 */
public interface WishService extends IBaseService<Wish> {
    public Wish getWish(int id);

    public Gift getGift(Wish wish);
}
