package com.wewish.service;

import com.wewish.entity_cms.CmsOrder;
import com.wewish.entity_tbl.TblOrder;

import java.util.List;

/**
 * Created by sjx on 15/7/21.
 */
public interface CmsOrderService extends IBaseService<CmsOrder> {
    List<CmsOrder> getAllCmsOrder();
    CmsOrder getCmsOrderByTblOrder(TblOrder tblOrder);
}
