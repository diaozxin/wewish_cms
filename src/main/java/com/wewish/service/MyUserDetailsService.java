package com.wewish.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * Created by diaozhengxin on 2015/6/30.
 */
@Service
public interface MyUserDetailsService extends UserDetailsService {
}
