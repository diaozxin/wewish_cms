package com.wewish.service;

import com.wewish.entity_tbl.Pagers;
import com.wewish.entity_tbl.TblOrder;
import com.wewish.entity_tbl.TblUser;

import java.util.List;

/**
 * Created by sjx on 15/7/13.
 */
public interface TblOrderService extends IBaseService<TblOrder>{

    public TblOrder load(int id);

    public TblOrder get(int id);

    /**
     * 以list形式返回所有的满足指定条件的TblOrder。条件由两个参数进行指定。
     * @param completed 为true表示返回已完成的TblOrder，为false表示未完成的TblOrder。
     * @param user 为null表示返回所有的TblOrder，不为null表示返回指定用户的TblOrder。
     * @return
     */
    public List<TblOrder> getOrderList(boolean completed, TblUser user);

    /**
     * 以Pager形式返回特定数量的满足指定条件的TblOrder。条件由completed和user两个参数进行指定。
     * @param pagesize 页大小
     * @param offset 页号，即当前是第几页
     * @param mobile 根据手机号进行查询，先用手机号获得user，再用user获得tblOrder，优先于user
     * @param completed 为true表示返回已完成的TblOrder，为false表示未完成的TblOrder。
     * @param user 为null表示结果包含所有用户的TblOrder，不为null表示返回指定用户的TblOrder。
     * @return
     */
    public Pagers<TblOrder> getOrderPager(TblUser user, boolean completed, String mobile, int pagesize, int offset);

}
