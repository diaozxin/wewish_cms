package com.wewish.service;

import com.wewish.entity_tbl.Address;

/**
 * Created by sjx on 15/7/14.
 */
public interface AddressService extends IBaseService<Address> {
    public Address getAddress(int id);
}
