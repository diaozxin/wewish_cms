package com.wewish.service;

import com.wewish.entity_tbl.TblUser;

/**
 * Created by sjx on 15/7/14.
 */
public interface TblUserService extends IBaseService<TblUser> {
    public TblUser getTblUser(int id);
    public TblUser getTblUserByMobile(String mobile);
}
