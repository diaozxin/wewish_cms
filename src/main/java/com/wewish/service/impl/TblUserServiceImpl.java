package com.wewish.service.impl;

import com.wewish.dao.TblUserDao;
import com.wewish.entity_tbl.TblUser;
import com.wewish.service.TblUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sjx on 15/7/14.
 */
@Service
public class TblUserServiceImpl extends TblBaseService<TblUser> implements TblUserService {
    @Autowired
    protected TblUserDao tblUserDao;

    @Override
    public TblUser getTblUser(int id){
        return tblUserDao.get(id);
    }

    @Override
    public TblUser getTblUserByMobile(String mobile){
        return tblUserDao.loadByMobile(mobile);
    }
}
