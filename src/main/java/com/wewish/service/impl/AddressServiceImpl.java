package com.wewish.service.impl;

import com.wewish.dao.AddressDao;
import com.wewish.entity_tbl.Address;
import com.wewish.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sjx on 15/7/14.
 */
@Service
public class AddressServiceImpl extends TblBaseService<Address> implements AddressService {
    @Autowired
    protected AddressDao addressDao;

    @Override
    public Address getAddress(int id){
        return addressDao.get(id);
    }
}
