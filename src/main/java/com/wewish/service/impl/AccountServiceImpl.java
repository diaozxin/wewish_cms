package com.wewish.service.impl;

import com.wewish.dao.AccountDao;
import com.wewish.entity_tbl.Account;
import com.wewish.entity_tbl.TblUser;
import com.wewish.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by sjx on 15/8/19.
 */
@Service
public class AccountServiceImpl extends TblBaseService<Account> implements AccountService{
    @Autowired
    private AccountDao accountDao;

    @Override
    public void updateAvailableOfTbluser(BigDecimal num, TblUser tblUser){
        Account account = accountDao.getAccountOfTbluser(tblUser);
        if(account!=null){
            account.setAvailable(account.getAvailable().add(num));
            account.setTotal(account.getTotal().add(num));
            accountDao.update(account);
        }
    }
}
