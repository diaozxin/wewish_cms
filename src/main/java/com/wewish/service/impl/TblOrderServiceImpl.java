package com.wewish.service.impl;

import com.wewish.dao.TblOrderDao;
import com.wewish.dao.TblUserDao;
import com.wewish.entity_tbl.Pagers;
import com.wewish.entity_tbl.TblOrder;
import com.wewish.entity_tbl.TblUser;
import com.wewish.service.TblOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sjx on 15/7/13.
 */
@Service
public class TblOrderServiceImpl extends TblBaseService<TblOrder> implements TblOrderService {

    @Autowired
    private TblOrderDao tblOrderDao;

    @Autowired
    private TblUserDao tblUserDao;

    @Override
    public TblOrder load(int id){
        return tblOrderDao.load(id);
    }

    @Override
    public TblOrder get(int id){
        return tblOrderDao.get(id);
    }

    @Override
    public List<TblOrder> getOrderList(boolean completed, TblUser user){
        if(null == user){
            if(completed) return tblOrderDao.getOrderCompletedList();
            else return tblOrderDao.getOrderList();
        }
        else{
            if(completed) return tblOrderDao.getOrderCompletedListByUser(user);
            else return tblOrderDao.getOrderListByUser(user);
        }
    }

    @Override
    public Pagers<TblOrder> getOrderPager(TblUser user, boolean completed, String mobile, int pagesize, int offset){
        offset = pagesize*(offset-1);
        if(!"".equals(mobile)){
            user = tblUserDao.loadByMobile(mobile);
        }
        if(null == user){
            if(completed) return tblOrderDao.getOrderCompletedPager(pagesize, offset);
            else return tblOrderDao.getOrderPager(pagesize, offset);
        }
        else{
            if(completed) return tblOrderDao.getOrderCompletedPagerByUser(pagesize, offset, user);
            else return tblOrderDao.getOrderPagerByUser(pagesize, offset, user);
        }
    }

}
