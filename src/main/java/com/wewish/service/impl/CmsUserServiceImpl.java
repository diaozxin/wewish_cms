package com.wewish.service.impl;

import java.util.List;
import com.wewish.dao.CmsUserDao;
import com.wewish.dao.RoleDao;
import com.wewish.dao.UserRoleDao;
import com.wewish.entity_cms.CmsUser;
import com.wewish.entity_cms.Role;
import com.wewish.entity_cms.RoleType;
import com.wewish.entity_cms.UserRole;
import com.wewish.service.CmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Zhengxin on 15/6/28.
 */
@Service
public class CmsUserServiceImpl extends BaseService<CmsUser> implements CmsUserService {

    @Autowired
    private CmsUserDao cmsUserDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserRoleDao userRoleDao;

    @Override
    public List<CmsUser> getList() {return cmsUserDao.cmsUserList();}

    @Override
    public CmsUser getUserByUsername(String username){
        return cmsUserDao.getUserByUsername(username);
    }

    @Override
    public List<Role> getRolesOfUser(int uid){
        return userRoleDao.listUserRoles(uid);
    }

    @Override
    public Role getRoleByRoletype(String roletype){
        RoleType tmp = RoleType.valueOf(roletype);
        return roleDao.getRoleByType(tmp);
    }

    @Override
    public void addUserRole(CmsUser cmsUser, Role role){
        UserRole userRole = userRoleDao.getByUserAndRole(cmsUser, role);
        if(userRole==null){
            userRole = new UserRole();
            userRole.setCmsUser(cmsUser);
            userRole.setRole(role);
            userRoleDao.add(userRole);
        }
    }

    @Override
    public void delUserRole(CmsUser cmsUser, Role role){
        UserRole userRole = userRoleDao.getByUserAndRole(cmsUser, role);
        if(userRole!=null){
            userRoleDao.delete(userRole.getId());
        }
    }
}
