package com.wewish.service.impl;

import com.wewish.dao.RoleDao;
import com.wewish.dao.CmsUserDao;
import com.wewish.dao.UserRoleDao;
import com.wewish.entity_cms.CmsUser;
import com.wewish.security.MyUserDetails;
import com.wewish.entity_cms.Role;
import com.wewish.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by diaozhengxin on 2015/6/30.
 */
@Service
public class MyUserDetailsServiceImpl implements MyUserDetailsService {

    @Autowired
    private CmsUserDao cmsUserDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CmsUser cmsUser = cmsUserDao.getUserByUsername(username);
        System.out.println(username);
        UserDetails userDetails = null;
        if(cmsUser != null ){
            userDetails = new MyUserDetails(username, cmsUser.getPassword(),
                    findUserAuthorities(cmsUser));
        }

        return userDetails;
    }

    public Collection<GrantedAuthority> findUserAuthorities(CmsUser cmsUser){
        List<GrantedAuthority> autthorities = new ArrayList<GrantedAuthority>();
        List<Role> roles = userRoleDao.listUserRoles(cmsUser.getId());

        for(Role role:roles){
            System.out.println(role.getRoleType().toString());
            autthorities.add(new SimpleGrantedAuthority(role.getRoleType().toString()));
        }
        return  autthorities;
    }
}
