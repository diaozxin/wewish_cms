package com.wewish.service.impl;

import com.wewish.dao.GiftDao;
import com.wewish.dao.WishDao;
import com.wewish.entity_tbl.Gift;
import com.wewish.entity_tbl.Wish;
import com.wewish.service.WishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sjx on 15/7/14.
 */
@Service
public class WishServiceImpl extends TblBaseService<Wish> implements WishService {
    @Autowired
    protected WishDao wishDao;

    @Autowired
    protected GiftDao giftDao;

    @Override
    public Wish getWish(int id){
        return wishDao.get(id);
    }

    @Override
    public Gift getGift(Wish wish){
        return giftDao.getGift(wish);
    }
}
