package com.wewish.service.impl;

import com.wewish.dao.IBaseDao;
import com.wewish.dao.Impl.BaseDaoImpl;
//import com.wewish.service.CacheService;
import com.wewish.service.IBaseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;

/**
 * Created by chenyuanjin on 15/6/19.
 */
@Service
public class BaseService<T> implements IBaseService<T> {

    private static Logger logger = Logger.getLogger(BaseService.class);

    @Autowired
    @Qualifier("baseDaoImpl")
    private IBaseDao<T> baseDao;

    @Override
    public void add(T t) {
        baseDao.add(t);
    }

    @Override
    public void update(T t) {

        baseDao.update(t);
    }

    @Override
    public void delete(int id) {
        baseDao.delete(id);
    }

    @Override
    public T load(int id) {
        return baseDao.load(id);
    }

    @Override
    public T get(int id) {
        return baseDao.get(id);
    }

    @Override
    public void saveOrUpdate(T t){
        baseDao.saveOrUpdate(t);
    }

}
