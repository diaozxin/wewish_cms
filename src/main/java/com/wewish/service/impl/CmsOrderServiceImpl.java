package com.wewish.service.impl;

import com.wewish.dao.CmsOrderDao;
import com.wewish.entity_cms.CmsOrder;
import com.wewish.entity_tbl.TblOrder;
import com.wewish.service.CmsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sjx on 15/7/21.
 */
@Service
public class CmsOrderServiceImpl extends BaseService<CmsOrder> implements CmsOrderService {
    @Autowired
    private CmsOrderDao cmsOrderDao;

    @Override
    public List<CmsOrder> getAllCmsOrder(){
        return cmsOrderDao.getAllCmsOrder();
    }

    @Override
    public CmsOrder getCmsOrderByTblOrder(TblOrder tblOrder) { return cmsOrderDao.getCmsOrderByTblOrder(tblOrder); }
}
