package com.wewish.service;

import com.wewish.entity_tbl.Account;
import com.wewish.entity_tbl.TblUser;

import java.math.BigDecimal;

/**
 * Created by sjx on 15/8/19.
 */
public interface AccountService extends IBaseService<Account> {
    public void updateAvailableOfTbluser(BigDecimal num, TblUser tblUser);
}
