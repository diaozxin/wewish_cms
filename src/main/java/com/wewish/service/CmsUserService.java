package com.wewish.service;

import com.wewish.entity_cms.CmsUser;
import com.wewish.entity_cms.Role;

import java.util.List;

/**
 * Created by Zhengxin on 15/6/28.
 */
public interface CmsUserService extends IBaseService<CmsUser> {

    List<CmsUser> getList();

    CmsUser getUserByUsername(String username);

    List<Role> getRolesOfUser(int uid);

    Role getRoleByRoletype(String roletype);

    void addUserRole(CmsUser cmsUser, Role role);

    void delUserRole(CmsUser cmsUser, Role role);
}
