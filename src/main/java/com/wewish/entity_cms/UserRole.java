package com.wewish.entity_cms;

import javax.persistence.*;

/**
 * Created by diaozhengxin on 2015/6/30.
 */

@Entity
@Table(name="cms_user_role")
public class UserRole {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name="u_id")
    private CmsUser cmsUser;

    @ManyToOne
    @JoinColumn(name="r_id")
    private Role role;

    public UserRole() {
    }

    public UserRole(CmsUser cmsUser, Role role) {
        this.cmsUser = cmsUser;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CmsUser getCmsUser() {
        return cmsUser;
    }

    public void setCmsUser(CmsUser cmsUser) {
        this.cmsUser = cmsUser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
