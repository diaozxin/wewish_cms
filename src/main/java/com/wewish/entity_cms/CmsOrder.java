package com.wewish.entity_cms;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wewish.tools.Json.JsonTimestampSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by sjx on 15/7/16.
 */

@Entity
@Table(name="cms_order")
public class CmsOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private Timestamp createTime;
    private String detail;
    private BigDecimal price;//really paid price
    private CmsUser cmsUser;
    private int tblOrderId;
    private int status;

    public CmsOrder(){

    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return this.id;
    }
    public void setId(int id){
        this.id = id;
    }

    @JsonSerialize(using=JsonTimestampSerializer.class)
    public Timestamp getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getDetail(){
        return this.detail;
    }
    public void setDetail(String detail){
        this.detail = detail;
    }

    public BigDecimal getPrice(){
        return this.price;
    }
    public void setPrice(BigDecimal price){
        this.price = price;
    }

    @ManyToOne
    public CmsUser getCmsUser(){
        return this.cmsUser;
    }
    public void setCmsUser(CmsUser cmsUser){
        this.cmsUser = cmsUser;
    }

    public int getTblOrderId(){
        return this.tblOrderId;
    }
    public void setTblOrderId(int tblOrderId){
        this.tblOrderId = tblOrderId;
    }

    public int getStatus() { return this.status; }
    public void setStatus(int s) { this.status = s; }
}
