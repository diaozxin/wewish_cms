package com.wewish.entity_cms;

import javax.persistence.*;

/**
 * Created by diaozhengxin on 2015/6/29.
 */
@Entity
@Table(name="cms_role")
public class Role {

    @Id
    @GeneratedValue
    private int id;
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name="role_type")
    private RoleType roleType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
