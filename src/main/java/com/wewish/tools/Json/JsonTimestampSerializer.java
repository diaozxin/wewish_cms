package com.wewish.tools.Json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by chenyuanjin on 15/6/17.
 */
public class JsonTimestampSerializer extends JsonSerializer<Timestamp> {

    private SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @Override
    public void serialize(Timestamp timestamp, JsonGenerator gen, SerializerProvider provider)
            throws IOException  {
        String value = dateFormat.format(timestamp);
        gen.writeString(value);
    }
}
