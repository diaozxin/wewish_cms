package com.wewish.tools;

/**
 * Created by chenyuanjin on 15/6/19.
 * 声明各种返回码常量
 */
public class ReturnCode {
    public static final int SUCCESS = 200;//操作成功
    public static final int TOKEN_EXPIRATE = 300;//token过期
    public static final int FAIL = 400;//操作失败
    public static final int ERROR = 500;//服务器错误
}
