package com.wewish.tools;


import java.util.ArrayList;

/**
 * Created by markwhat on 15/8/1.
 */
public class OrderStatus {
    public static final int STATUSMIN = 0;
    public static final int STATUSMAX = 9;
    public static final int UNINITED  = 0;//客户未下单
    public static final int RECEIVED  = 1;//获得订单信息，等待后台处理，进行中，或因无效信息沟通中
    public static final int INVALID   = 2;//订单收货信息不正确，沟通无效，无法下单，已取消
    public static final int NOTENOUGH = 3;//实际价格高于预算价格，沟通无效，无法下单，已取消
    public static final int ORDERED   = 4;//已人工下单，不能再修改订单信息，进行中
    public static final int SENT      = 5;//已发货，进行中
    public static final int SENDBACK  = 6;//用户退换货中，进行中
    public static final int COMPLETED = 7;//已完成
    public static final int OTHERFAIL = 8;//其他失败结果，已取消
    public static final int PAIDBACK  = 9;//已退款，最终应跳转到7

    public static final String[] describe = {
            "客户未下订单",
        "客户刚下订单",
        "客户信息不正确，取消",
        "商品实际价格过高，取消",
        "已为客户下单，等待卖家发货",
        "卖家已发货，等待客户接收",
        "用户退换货中",
        "收货正常完成",
        "其他失败结果，取消",
        "取消成功，已退款"
    };

    public static final int trans[][] = {
            {0,1,0,0,0,0,0,0,0,0},//0
            {0,0,1,1,1,0,0,0,1,0},//1
            {0,0,0,0,0,0,0,0,0,1},//2
            {0,0,0,0,0,0,0,0,0,1},//3
            {0,0,0,0,0,1,0,0,0,0},//4
            {0,0,0,0,0,0,0,1,0,0},//5
            {0,0,0,0,0,0,0,0,1,1},//6
            {0,0,0,0,0,0,1,0,0,0},//7
            {0,0,0,0,0,0,0,0,0,1},//8
            {0,0,0,0,0,0,0,1,0,0} //9
    };

    public static boolean isStatusValid(int s){
        if(s < STATUSMIN || s > STATUSMAX)
            return false;
        return true;
    }

    public static boolean canTransTo(int from, int to){
        if(from < STATUSMIN || from > STATUSMAX || to < STATUSMIN || to > STATUSMAX) {
            return false;
        }
        return (trans[from][to] == 1);
    }

    public static String getStatusDescribe(int s){
        if(s < STATUSMIN || s > STATUSMAX)
            return "ERROR: status "+s+" not valid.";
        return describe[s];
    }

    public static ArrayList<Integer> getPosibleNextStatus(int s){
        ArrayList<Integer> nss = new ArrayList<Integer>();
        if(s < STATUSMIN || s > STATUSMAX)
            return nss;
        int i = STATUSMIN;
        for(; i <= STATUSMAX; i++){
            if(trans[s][i] == 1){
                nss.add(i);
            }
        }
        return nss;
    }

    public static ArrayList<String> getPosibleNextStatusStr(int s){
        ArrayList<String> nsss = new ArrayList<String>();
        if(s < STATUSMIN || s > STATUSMAX)
            return nsss;
        int i = STATUSMIN;
        for(; i <= STATUSMAX; i++){
            if(trans[s][i] == 1){
                nsss.add(describe[i]);
            }
        }
        return nsss;
    }

}


