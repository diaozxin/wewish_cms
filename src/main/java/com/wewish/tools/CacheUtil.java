package com.wewish.tools;

/**
 * Created by chenyuanjin on 15/6/18.
 */
public class CacheUtil {
    public static final  int  SECOND = 1;
    public static final  int  THIRTY_SECOND = 30*SECOND;
    public static final  int  MINUTE = 60*SECOND;
    public static final  int TEN_MINUTE = 10*MINUTE;
    public static final  int THIRTY_MINUTE = 30*MINUTE;
    public static final  int HOUR= 60*MINUTE;
    public static final  int DAY = 24*HOUR;
    public static final  int MONTH = 30*DAY;

    public static final String PHONE_KEY_PREFIX = "PHONE_";

    public static String getKey(Class c,Object id){
        return   c.getCanonicalName()+"."+id.toString();
    }

    public static String getKey(String keyPrefix,Object id){
        return   keyPrefix +id.toString();
    }
}
