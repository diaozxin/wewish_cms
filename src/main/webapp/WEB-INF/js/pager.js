/**
 * Created by sjx on 15/8/30.
 */
/**
 * 获得页码信息的html代码，从而将其直接填入页面某个位置来显示
 * @param pagesize 一页中包含多少条消息
 * @param offset 当前显示的是第几页（从1开始）
 * @param pageCount 一共有多少页消息
 * @param url 携带pagesize和offset两个参数访问该地址，从而得到特定页的所有消息
 * @returns {string} 返回html代码，直接填入页面即可
 */
function getPageInfo(pagesize, offset, pageCount, url) {
    var intPage = 7;  //最多显示的页数
    var intBeginPage = 0;//开始的页数
    var intEndPage = 0;//结束的页数
    var intCrossPage = parseInt(intPage / 2); //最多显示页数的一半
    var strPage = "<ul class='pagination pagination-right'><li class='disabled'> <a href='javascript:void(0);'>第" + offset + "/" + pageCount + " 页 每页 " + pagesize + " 条</a> &nbsp;&nbsp;&nbsp;&nbsp;</li>";
    if (offset > 1) {
        strPage = strPage + "<li><a class='page' href='" + url + "offset=1&pagesize=" + pagesize + "'>首页</a></li> ";
        strPage = strPage + "<li><a class='page' href='" + url + "offset=" + (offset - 1) + "&pagesize=" + pagesize + "'>上一页</a></li> ";
    }
    if (pageCount > intPage) {//总页数大于在页面显示的页数
        if (offset > pageCount - intCrossPage) {//当前页数>总页数-3
            intBeginPage = pageCount - intPage + 1;
            intEndPage = pageCount;
        }
        else {
            if (offset <= intPage - intCrossPage) {
                intBeginPage = 1;
                intEndPage = intPage;
            }
            else {
                intBeginPage = offset - intCrossPage;
                intEndPage = offset + intCrossPage;
            }
        }
    } else {
        intBeginPage = 1;
        intEndPage = pageCount;
    }
    if (pageCount > 0) {
        for (var i = intBeginPage; i <= intEndPage; i++) {
            {
                if (i == offset) {//当前页
                    strPage = strPage + " <li><a class='active' href='javascript:void(0);'>" + i + "</a></li> ";
                }
                else {
                    strPage = strPage + " <li><a class='page' href='" + url + "offset=" + i + "&pagesize=" + pagesize + "' title='第" + i + "页'>" + i + "</a></li> ";
                }
            }
        }
    }
    if (offset < pageCount) {
        strPage = strPage + "<li><a class='page' href='" + url + "offset=" + (offset + 1) + "&pagesize=" + pagesize + "'>下一页</a></li> ";
        strPage = strPage + "<li><a class='page' href='" + url + "offset=" + pageCount + "&pagesize=" + pagesize + "'>尾页</a></li> ";
    }
    return strPage+"</ul>";
}