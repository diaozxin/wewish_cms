<html>
<head>
    <title>下单记录</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">
<#include "./admin/head.ftl">
<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>
            订单信息
        </h1>
    </div>
    <p>管理订单订单ID：${order.id}</p>
    <p>用户ID：${order.user.id}</p>
    <p>订单价格：${order.price}</p>
    <p>用户昵称：${order.user.nickname}</p>
    <p>用户手机号码：${order.user.mobile}</p>
    <p>礼物链接：<a href="${gift.url}">${gift.url}</a></p>
    <p>订单描述：${order.description}</p>
    <p>订单详情：${order.detail}</p>
    <p>收货地址：${order.address.address}</p>
    <p>订单状态：${status}:${str_status}</p>
    <p></p>
    <form action="/orderlist/orderbought" method="get">
        <input type="text" hidden="hidden" name="id" value="${order.id}">
        实际下单价格：
        <div class="form-group">
            <input type="text" name="price" value="${paidprice}" class="form-control">
        </div></b>
        备注：
        <div class="form-group">
            <input type="text" name="detail" placeholder="" class="form-control" value="${detail}">
        </div></b>
        <div>
            <table class="table">
            <tr>
            <#list nextstatus as nstatus>
                <th>
                    <input type="radio" name="nextstatus" value="${nstatus}" />
                </th>
            </#list>
            </tr><tr>
            <#list nextstatus_str as nstatus_str>
                <td>${nstatus_str}</td>
            </#list>
            </tr>
            </table>
        </div>
        <button type="submit" class="btn btn-success">提交</button>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>


    <script src="/js/jquery-2.1.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
</div>
</body>
</html>