<html>
<head>
    <title>全部订单</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="/js/jquery-2.1.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">
<#include "./admin/head.ftl">
<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>
            已完成订单
        </h1>
        <a href="/orderlist">查看待处理订单</a>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table-bordered">
                <thead>
                <tr>
                    <th>订单id</th>
                    <th>用户id</th>
                    <th>用户昵称</th>
                    <th>手机号码</th>
                    <th>筹集金额</th>
                    <th>礼物链接</th>
                    <th>订单描述</th>
                    <th>订单详情</th>
                    <th>收货地址</th>
                    <th>订单状态</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <#list orderList as tblOrder>
                <tr><td>${tblOrder.order.id}</td>
                    <td>
                        <a href="/orderlist/selectbyid?id=${tblOrder.order.user.id}">
                        ${tblOrder.order.user.id}
                        </a>
                    </td>
                    <td>${tblOrder.order.user.nickname}</td>
                    <td>${tblOrder.order.user.mobile}</a>
                    </td>
                    <td>${tblOrder.order.price}</td>
                    <td><a target="_blank" href="http://${tblOrder.gift.url}">${tblOrder.gift.url}</a></td>
                    <td>${tblOrder.order.description}</td>
                    <td>${tblOrder.order.detail}</td>
                    <td>${tblOrder.order.address.address}</td>
                    <td>${tblOrder.order.status}${tblOrder.status}</td>
                    <td><button type="button" class="btn btn-info" onclick=
                            "window.location.href='/orderlist/orderboughtform?id=${tblOrder.order.id?c}'">详情</button></td>
                </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </div>

</div>
</body>
</html>