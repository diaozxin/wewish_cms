<html>
<head>
    <title>cms users</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="../../css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="/js/jquery-2.1.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="application/javascript">
        var xmlhttp;
        function isSuccess(){
            if(xmlhttp.readyState!=4) return;
            if(xmlhttp.status!=200)
            {
                alert("访问失败");
                location.reload(true);
            }
            var ret = JSON.parse(xmlhttp.responseText);
            if(ret.status != 200)
            {
                alert("操作失败");
                location.reload(true);
            }
        }
        function changeRole(box, username, role){
            xmlhttp=null;
            if(window.XMLHttpRequest)
                xmlhttp = new XMLHttpRequest();
            else if(window.ActiveXObject)
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            if (xmlhttp != null)
            {
                if($(box).is(":checked")){
                    xmlhttp.open('POST', '/admin/add_role/', true);
                }
                else{
                    xmlhttp.open('POST', '/admin/del_role/', true);
                }
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send('user='+username+'&role='+role+'&${_csrf.parameterName}=${_csrf.token}');
                xmlhttp.onreadystatechange = isSuccess;
            }
        }
    </script>
</head>
<body>
<#include "head.ftl">
<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>
            CMS USERS
        </h1>
    </div>
    <div class="row">
        <div class="col-md-6">
            <table class="table">
                <thead>
                    <tr>
                        <th>用户名</th>
                        <th>密码</th>
                        <th>ROLE_ADMIN</th>
                        <th>ROLE_USER</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <#list cmsuserlist as cmsuser>
                    <tr>
                        <td>${cmsuser.cmsuser.username}</td>
                        <td>${cmsuser.cmsuser.password}</td>
                        <#if cmsuser.is_admin>
                            <td><input type="checkbox" checked="checked" onclick="changeRole(this, '${cmsuser.cmsuser.username}','ROLE_ADMIN')"/></td>
                        <#else>
                            <td><input type="checkbox" onclick="changeRole(this, '${cmsuser.cmsuser.username}','ROLE_ADMIN')"/></td>
                        </#if>

                        <#if cmsuser.is_user>
                            <td><input type="checkbox" checked="checked" onclick="changeRole(this, '${cmsuser.cmsuser.username}','ROLE_USER')"/></td>
                        <#else>
                            <td><input type="checkbox" onclick="changeRole(this, '${cmsuser.cmsuser.username}','ROLE_USER')"/></td>
                        </#if>
                    </tr>
                    </#list>
                </tbody>
            </table>
        </div>
    </div>
    <input type="button" class="btn-primary" onclick="location.assign('/admin/add_cmsuser')" value="添加用户"/>
</div>

</body>
</html>