<html>
<head>
    <title>添加用户</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="/js/jquery-2.1.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <script type="application/javascript">
        var xmlhttp;
        function isSuccess(){
            if(xmlhttp.readyState!=4) return;
            if(xmlhttp.status!=200)
            {
                alert("访问失败");
                return;
            }
            var ret = JSON.parse(xmlhttp.responseText);
            if(ret.status != 200)
            {
                alert("已经存在的用户名");
                return;
            }
            else{
                alert("用户创建成功");
                location.href = "/admin/cmsusers";
            }
        }
        function mySubmit(){
            xmlhttp=null;
            if(window.XMLHttpRequest)
                xmlhttp = new XMLHttpRequest();
            else if(window.ActiveXObject)
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            if (xmlhttp != null)
            {
                xmlhttp.open('POST', '/admin/addcmsuser/', true);
                xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                xmlhttp.send('username='+$('#username').val()+'&password='+$('#password').val()+
                        '&state='+$('#state').val()+'&${_csrf.parameterName}=${_csrf.token}');
                xmlhttp.onreadystatechange = isSuccess;
            }
        }
    </script>

</head>

<body role="document">
<#include "head.ftl">
<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>
            添加用户信息
        </h1>
    </div>
    <form action="/admin/addcmsuser" method="post">
        username：
        <div class="form-group">
            <input type="text" id="username" class="form-control">
        </div></b>
        password：
        <div class="form-group">
            <input type="password" id="password" class="form-control">
        </div></b>
        state：
        <div class="form-group">
            <input type="text" id="state" placeholder="用户状态...瞎填一个数字即可" class="form-control">
        </div></b>
        <button type="button" class="btn btn-success" onclick="mySubmit()">提交</button>
    </form>

</div>
</body>
</html>