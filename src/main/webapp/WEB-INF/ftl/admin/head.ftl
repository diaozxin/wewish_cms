<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="/" class="navbar-brand">WeWesh CMS</a>
        </div>
        <nav id="bs-navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/orderlist">订单管理</a>
                </li>
                <li>
                    <a href="/clientaccount">客户账户</a>
                </li>
                <li>
                    <a href="/loglist">日志管理</a>
                </li>
                <li>
                    <a href="/pushtab">推送管理</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="/login">登录</a></li> -->
            </ul>
        </nav>
    </div>
</div>
<div style="height: 40px;"><p></p></div>