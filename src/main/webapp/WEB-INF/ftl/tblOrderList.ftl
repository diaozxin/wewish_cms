<html>
<head>
    <title>全部订单</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>tblOrder List</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="/js/jquery-2.1.4.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/pager.js"></script>
    <script type="application/javascript">
        window.onload = function(){
            var pagesize = ${pageInfo.pageSize}; //js中可以使用freemarker
            var offset = ${pageInfo.offSet};
            var total = ${pageInfo.total};
            var idReg = new RegExp("id=([^&]*)(&|$)");
            var completedReg = new RegExp("completed=([^&]*)(&|$)");
            var mobileReg = new RegExp("mobile=([^&]*)(&|$)");
            var id = location.href.match(idReg)[1];
            var completed = location.href.match(completedReg)[1];
            var mobile = location.href.match(mobileReg)[1];
            $("#mobile").val(mobile);
            $("#pageinfo").html(getPageInfo(pagesize, offset/pagesize+1,Math.ceil(total/pagesize), "/orderlist/pager?id="+id+"&completed="+completed+"&mobile="+mobile+"&"));
        }
        function queryByMobile(){
            var mobile = $("#mobile").val();
            location.assign('/orderlist/pager?id=&completed=&mobile='+mobile+'&pagesize=&offset=');
        }
    </script>
</head>

<body role="document">
<#include "./admin/head.ftl">
<div class="container theme-showcase" role="main">
    <div class="page-header">
        <h1>
            待处理订单
        </h1>
        <button type="button" class="btn btn-link" onclick="location.assign('/orderlist/pager?id=&completed=true&mobile=&pagesize=&offset=')">
            查看已完成订单
        </button>
        <input type="text" id="mobile" placeholder="输入手机号">
        <button type="button" class="btn btn-primary" onclick="queryByMobile()">查询</button>
    </div>

    <div class="row">
        <div class="col-md-12">
        <table class="table-bordered">
            <thead>
                <tr>
                    <th>订单id</th>
                    <th>用户id</th>
                    <th>用户昵称</th>
                    <th>手机号码</th>
                    <th>筹集金额</th>
                    <th>礼物链接</th>
                    <th>订单描述</th>
                    <th>订单详情</th>
                    <th>收货地址</th>
                    <th>订单状态</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="tbody">
                <#list orderList as tblOrder>
                <tr><td>${tblOrder.order.id}</td>
                    <td>
                        <a href="/orderlist/pager?id=${tblOrder.order.user.id}&completed=&mobile=&pagesize=&offset=">
                        ${tblOrder.order.user.id}
                        </a>
                    </td>
                    <td>${tblOrder.order.user.nickname}</td>
                    <td>${tblOrder.order.user.mobile}</a>
                    </td>
                    <td>${tblOrder.order.price}</td>
                    <td><a target="_blank" href="http://${tblOrder.gift.url}">${tblOrder.gift.url}</a></td>
                    <td>${tblOrder.order.description}</td>
                    <td>${tblOrder.order.detail}</td>
                    <td>${tblOrder.order.address.address}</td>
                    <td>${tblOrder.order.status}${tblOrder.status}</td>
                    <td><button type="button" class="btn btn-info" onclick=
                            "location.href='/orderlist/orderboughtform?id=${tblOrder.order.id?c}'">记账</button></td>
                </tr>
                </#list>
            </tbody>
        </table>
        </div>
    </div>
    <div class="row" id="pageinfo"></div>
</div>
</body>
</html>