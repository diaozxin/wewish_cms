<%--
  Created by IntelliJ IDEA.
  CmsUser: diaozhengxin
  Date: 2015/7/1
  Time: 13:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8" %>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>
<head>
  <title>WeWish Cms</title>
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="0">
  <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
  <meta http-equiv="description" content="This is my page">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/signin.css" rel="stylesheet">
  <!-- Bootstrap theme -->
  <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
  <script src="/js/jquery-2.1.4.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/bootstrap.min.js"></script>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <script type="application/javascript">
    $(document).ready(function(){
      var e = document.location.href.indexOf('error=');
      if(e != -1 && document.location.href.slice(e + 6) == 'true')
          document.getElementById('error').innerHTML = '用户名密码错误';
    });
  </script>
</head>
<body>
<div class="container">
  <form class="form-signin" action="<%=basePath%>login" method="POST">
    <h2 class="form-signin_heading">用户登录</h2>
    <div id="error"></div>
    <label for="user-name-label" class="sr-only">用户名</label>
    <input type="text" id="user-name-label" class="form-control" placeholder="用户名" name="username" required autofocus>
    <label for="inputPassword" class="sr-only">密码</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="密码" name="password" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

  </form>
</div>
  <%--<h3>用户登录</h3>--%>
  <%--<form class="login_from" action="<%=basePath%>login" method="POST">--%>
    <%--<p>--%>
      <%--<input id="username" name="username" type="text">--%>
    <%--</p>--%>
    <%--<p>--%>
      <%--<label for="password">Password</label><input id="password" name="password" type="password">--%>
    <%--</p>--%>
    <%--<input type="submit" name="submit" value="Login"/>--%>
    <%--<div id="error"></div>--%>
    <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
  <%--</form>--%>

</body>
</html>
